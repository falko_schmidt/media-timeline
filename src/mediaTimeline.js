/**
 * Create a new media-timeline based on the timeline from [vis]{@link http://visjs.org/}
 * 
 * @class
 * @param {DomElement} container 
 *        The Dom element aimed to contain the timeline
 * @param {PlayerAdapter} player  
 *        The player adapter to bind with the timeline {@link PlayerAdapter}
 * @param {Array} data      
 *        An array of [vis items]{@link http://visjs.org/docs/timeline.html#items}
 * @param {Array} groups    
 *        An array of [vis groups]{@link http://visjs.org/docs/timeline.html#groups}
 * @param {Object} options   
 *        [Timeline options, see {@link http://visjs.org/docs/timeline.html#Configuration_Options}]
 * @return {{@link vis.Timeline}} A vis.Timeline extended with the given player adapter (use getPlayer to access it).
 */
function MediaTimeline (container, player, data, groups, options) {
        'use strict';

        var timeline,
            start,
            end,
            ready = false,
            self = this;


        if (typeof vis === 'undefined' || typeof vis.Timeline === 'undefined') {
            throw 'The vis timeline is required for the MediaTimeline library.';
        }

        /**
         * Generate a date based on the time given in hours, minutes, seconds.
         * 
         * The date will start with the Thu Jan 01 1970 01:00:00 GMT+0100 (CET)
         * + the given time. It should be only used with this timeline. The day,
         * month and year are useless here.
         * 
         * @param  {number} hours   The hours from the given time 
         * @param  {number} minutes The minutes from the given time
         * @param  {number} seconds The seconds from the given time
         * @return {Date}        The generate 
         */
        function generateDateFromTime (hours, minutes, seconds) {
            return new Date((((((hours * 60) + minutes) * 60) + seconds) * 1000));
        }

        /**
         * Transform the given date into a time in seconds
         * 
         * @param {Date} date The formated date from timeline
         * @returns {number} Date converted to time in seconds
         */
        function getTimeInSeconds (date) {
                var time = (date.getHours() - 1) * 3600 + date.getMinutes() * 60 + date.getSeconds() + date.getMilliseconds() / 1000;
                return Math.round(Number(time)); // Ensue that is really a number
        }

        /**
         * Callback for the player timeupdate
         */
        this.onTimeUpdate = function () {
            var newDate = generateDateFromTime(0, 0, player.getCurrentTime());
            timeline.setCustomTime(newDate);
        };

        /**
         * Callback for the item selection on timeline
         * @param  {Event} event Object containing the array of selected items 
         */
        this.onItemSelection = function (event) {
            var newSelection = timeline.itemsData.get(event.items[0]);

            if (!newSelection.noselection) {
                player.setCurrentTime(getTimeInSeconds(newSelection.start));
            }
        };

        /**
         * Callback for the timeline timeupdate
         * @alias module:views-timeline.TimelineView#onTimelineMoved
         * @param {Event} event Event object
         */
        this.onTimelineMoved = function (event) { 
            var newTime = getTimeInSeconds(event.time),
                hasToPlay = (player.getStatus() === PlayerAdapter.STATUS.PLAYING);


            if (hasToPlay) {
                player.pause();
            }

            player.setCurrentTime((newTime < 0 || newTime > player.getDuration()) ? 0 : newTime);

            if (hasToPlay) {
                player.play();
            }
        };

        /**
         * Constructor for the new timeline
         */
        function init (container, player, data, groups, options) {
            var onPlayerReady = function () {
                end = generateDateFromTime(0, 0, player.getDuration());

                if (typeof timeline !== 'undefined') {
                    player.removeEventListener(PlayerAdapter.EVENTS.READY , onPlayerReady);
                    options.end = end;
                    options.max = end;
                    timeline.setOptions(options);
                }

                player.addEventListener(PlayerAdapter.EVENTS.TIMEUPDATE, self.onTimeUpdate);
                ready = true;
            };
            
            start = generateDateFromTime(0, 0, 0);

            if (player.getStatus() === PlayerAdapter.STATUS.INITIALIZING) {
                end = start;
                player.addEventListener(PlayerAdapter.EVENTS.READY , onPlayerReady);
            } else {
                onPlayerReady();
            }

            options.start           = start;
            options.end             = end;
            options.min             = start;
            options.max             = end;
            options.type            = 'range';
            options.showCurrentTime = false;
            options.stack           = false;
            options.margin          = {
                item: 1
            }; 
            options.showCustomTime  = true;
            options.showMajorLabels = false;
            options.zoomMax         = 10000000;
            
            timeline = new vis.Timeline(container, new vis.DataSet(data), new vis.DataSet(groups), options);
            timeline.setCustomTime(start);
            timeline.on('timechange', self.onTimelineMoved);
            timeline.on('timechanged', self.onTimelineMoved);
            timeline.on('select', self.onItemSelection);

            timeline.getPlayer = function () {
                return player;
            };

            return timeline;
        }

        return init(container, player, data, groups, options);
}